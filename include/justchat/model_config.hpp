#ifndef MODEL_CONFIG_HPP
#define MODEL_CONFIG_HPP
#include <string>
#include <optional>
#include <commoncpp/config.hpp>


namespace LM {
namespace Chat {
class ModelConfig final : public common::Configuration {
protected:
    void fill(KeyValueMap&&, bool ignore_extra = false) override;
    void check() const override;

public:
    struct Prompt {
        std::string prefix = "### Human:\n",
                    suffix = "\n### Assistant:\n";
    };

    ModelConfig() {
        ignore_environment = true;
    }

    void parse(const std::string& file) override;

    std::string config_file;

    std::string model_file,
                language = "en";
    Prompt mutable prompt;
    bool allow_system_prompt = true,
         strict_prompt = false,
         emits_eos = true; // Weather the model emits an eos at the end of each response

    std::optional<unsigned> max_context_size,
                            repeat_last, // How many tokens to repeat-penalize
                            top_k,
                            mirostat_version; // Version of mirostat to use; 0 for none
    std::optional<float> top_p,
                         temp,
                         mirostat_learning_rate,
                         mirostat_target_entropy,
                         repeat_penalty;
};
}
}
#endif // MODEL_CONFIG_HPP
