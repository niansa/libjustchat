#ifndef CHAT_HPP
#define CHAT_HPP
#include "model_config.hpp"

#include <string>
#include <string_view>
#include <memory>
#include <functional>
#include <justlm.hpp>
#include <justlm_pool.hpp>


namespace LM {
namespace Chat {
class Inference {
    ModelConfig config;

    std::unique_ptr<LM::Inference> inference = nullptr;
    LM::InferencePool *pool = nullptr;

    size_t get_id() const {
        return static_cast<size_t>(reinterpret_cast<uintptr_t>(this)); // Dirty hack that just works
    }

public:
    using GenerateCallback = std::function<bool(std::string_view)>;
    using EvaluateCallback = std::function<bool(float, bool)>;

    class OptionallySharedInference {
        std::shared_ptr<LM::Inference> s_ptr;
        LM::Inference *r_ptr;

    public:
        OptionallySharedInference() : r_ptr(nullptr) {}
        OptionallySharedInference(std::shared_ptr<LM::Inference> &&s)
              : s_ptr(std::move(s)) {
            r_ptr = s_ptr.get();
        }
        OptionallySharedInference(LM::Inference *r)
              : r_ptr(std::move(r)) {}

        LM::Inference *operator ->() const {
            return r_ptr;
        }
    };

    Inference(const std::string& config_path);
    Inference(LM::InferencePool& pool, const std::string& config_path);

    LM_SCHEDULABLE(bool) reset();
    LM_SCHEDULABLE(bool) append(const ModelConfig::Prompt& promptConfig, const std::string& message, const EvaluateCallback& on_evaluate = nullptr);
    LM_SCHEDULABLE(bool) append(const std::string& message, const EvaluateCallback& on_evaluate = nullptr);
    LM_SCHEDULABLE(std::string) generate(const ModelConfig::Prompt& promptConfig, const GenerateCallback& on_generate = nullptr);
    LM_SCHEDULABLE(std::string) generate(const GenerateCallback& on_generate = nullptr);
    LM_SCHEDULABLE(std::string) prompt(const std::string& message, const GenerateCallback& on_generate = nullptr, const EvaluateCallback& on_evaluate = nullptr);
    LM_SCHEDULABLE(std::string) prompt(const ModelConfig::Prompt& promptConfig, const std::string& message, const GenerateCallback& on_generate = nullptr, const EvaluateCallback& on_evaluate = nullptr);

    LM_SCHEDULABLE(bool) start() {
        LM_CORETURN LM_COAWAIT reset();
    }

    LM_SCHEDULABLE(OptionallySharedInference) get_underlaying() const;

    LM::Inference::Params get_params() const;

    const ModelConfig& get_config() const {
        return config;
    }
};
}
}
#endif // CHAT_HPP
